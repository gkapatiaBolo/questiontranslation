import csv
import os

from indictrans import Transliterator as IndicTransliterator
from polyglot.transliteration import Transliterator as PolyglotTransliterator
from indic_transliteration import sanscript
from indic_transliteration.sanscript import transliterate
from polyglot.detect import Detector
from langdetect import detect

import requests
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

indic_transliterator = IndicTransliterator(source='hin', target='eng', build_lookup=True)
polyglot_transliterator = PolyglotTransliterator(source_lang="hi", target_lang="en")
GOOGLE_TRANSLATION_API_URL = "https://translation.googleapis.com/language/translate/v2"
GOOGLE_TRANSLATION_API_KEY = os.environ['GOOGLE_TRANSLATION_API_KEY']

inputQuestions = []


def polyglot_sentence_transliteration(sentence):
    output = ''
    for word in sentence.split():
        polyglot_detect = 'un'
        try:
            polyglot_detect = Detector(word).language.code
        except Exception as ex:
            logger.error(str(ex))

        if polyglot_detect == 'en':
            output += word + ' '
        elif polyglot_detect == 'hi':
            polyglot_transliterated = polyglot_transliterator.transliterate(word)
            if polyglot_transliterated == '':
                output += word + ' '
            else:
                output += polyglot_transliterated + ' '
        else:
            output += word + ' '
    return output.strip()


class ProcessedQuestion:
    def __init__(self, id, original):
        self.id = id
        self.original = original
        self.transliterated = None
        self.detected_language = None
        self.translated = None
        ProcessedQuestion.set_detected_language(self)
        ProcessedQuestion.set_transliterated(self)
        ProcessedQuestion.set_translated(self)

    def set_transliterated(self):
        self.transliterated = []
        self.transliterated.append(indic_transliterator.transform(self.original))
        self.transliterated.append(polyglot_sentence_transliteration(self.original))
        self.transliterated.append(transliterate(self.original, sanscript.DEVANAGARI, sanscript.ITRANS).lower())
        self.transliterated.append(transliterate(self.original, sanscript.DEVANAGARI, sanscript.IAST).lower())

    def set_detected_language(self):
        self.detected_language = detect(self.original)

    def set_translated(self):
        data = {'q': self.original,
                'source': self.detected_language,
                'target': 'hi' if self.detected_language == 'en' else 'en',
                'format': 'text'}
        try:
            output = post(GOOGLE_TRANSLATION_API_URL, GOOGLE_TRANSLATION_API_KEY, data)
            self.translated = output['data']['translations'][0]['translatedText']
            # self.translated = "translate, check logs"
        except Exception as ex:
            logger.error(str(ex))
            self.translated = "could not translate, check logs"

    def __str__(self):
        return f'id: {self.id},\n' \
               f'original: {self.original},\n' \
               f'detected language: {self.detected_language},\n' \
               f'transliterated: {self.transliterated},\n' \
               f'translated: {self.translated}\n\n'


class InputQuestion:
    def __init__(self, id, username, text, topics, language):
        self.id = id
        self.username = username
        self.text = text
        self.topics = topics
        self.language = language


def post(url, key, data):
    response = requests.post(url=url + "?key=" + key, data=data)
    json_data = response.json()
    return json_data


def read_data():
    with open('questions.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                inputQuestions.append(InputQuestion(row[0], row[1], row[2], row[3], row[4]))
                line_count += 1
        logger.info(f'Processed {line_count} lines.')


def write_output(text):
    try:
        f = open("output.txt", "a")
        f.write(str(text) + "\n\n")
        f.close()
    except Exception as ex:
        logger.error(f"Cold not save file with exception {str(ex)}")


read_data()
for inputQuestion in inputQuestions:
    output = ProcessedQuestion(inputQuestion.id, inputQuestion.text)
    write_output(str(output))
